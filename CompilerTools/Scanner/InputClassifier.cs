﻿using System;
using System.Collections.Generic;

namespace CompilerTools.Scanner
{
    /// <summary>
    /// Classifies a character into a Classification Group
    /// </summary>
    public static class InputClassifier
    {
        /// <summary>
        /// Collection of characters that will be classified as Whitespace
        /// </summary>
        /// <value>The white spaces.</value>
        public static ICollection<char> WhiteSpaces { get { return new HashSet<char> (_WhiteSpaces); } }
        private static readonly ICollection<char> _WhiteSpaces = new HashSet<char>
        {
            '\n', '\t', ' '
        };

        /// <summary>
        /// Classify the given character into its Classification group
        /// </summary>
        /// <param name="c">The character to classify</param>
        public static Classification Classify(char c)
        {
            int val = (int)c;

            //if (val < 0 || val > 127)
            //    return Classification.Invalid;

            // Check Whitespace first
            if (WhiteSpaces.Contains (c))
                return Classification.Whitespace;

            if (val >= 0 && val <= 31)
                return Classification.Invisible;

            if (val >= 32 && val <= 47)
                return Classification.Punctuation;

            if (val >= 48 && val <= 57)
                return Classification.Numeric;

            if (val >= 58 && val <= 64)
                return Classification.Punctuation;

            if (val >= 65 && val <= 90)
                return Classification.Character;

            if (val >= 91 && val <= 96)
                return Classification.Punctuation;

            if (val >= 97 && val <= 122)
                return Classification.Character;

            if (val >= 123 && val <= 126)
                return Classification.Punctuation;

            return (val == 127)
                ? Classification.Invisible
                : Classification.Invalid;
        }
    }

    /// <summary>
    /// An enumeration of possible ascii character classifications
    /// </summary>
    public enum Classification
    {
        Character,
        Numeric,
        Punctuation,
        Invisible,
        Invalid,
        Whitespace
    }
}

