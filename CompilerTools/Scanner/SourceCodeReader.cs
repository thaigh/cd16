﻿using System;
using System.IO;
using System.Text;

namespace CompilerTools.Scanner
{
    /// <summary>
    /// A reader API for reading source code documents
    /// </summary>
    public class SourceCodeReader
    {
        private string _sourceCodeInput;
        private int _nextCharIndex = 0;
        private string [] _lines;

        /// <summary>
        /// Gets the current line in the source code file
        /// </summary>
        /// <value>The line.</value>
        public int Line { get; private set; }

        /// <summary>
        /// Gets the column in the source code line
        /// </summary>
        /// <value>The column.</value>
        public int Column { get; private set; }

        /// <summary>
        /// Gets the current line being read by the source code reader
        /// </summary>
        /// <remarks>
        ///     Strings are immutable in C#, so we can return
        ///     directly from the reader without cloning
        /// </remarks>
        /// <value>The current line.</value>
        public string CurrentLine { get { return _lines [Line - 1]; } }

        public SourceCodeReader (FileInfo file) : this (file.OpenText()) { }
        public SourceCodeReader (StreamReader stream) : this (stream.ReadToEnd ()) { }
        public SourceCodeReader (string sourceCodeInput)
        {
            _sourceCodeInput = sourceCodeInput;
            CleanseSourceCode ();

            Line = 1;
            Column = 1;

            _lines = _sourceCodeInput.Split ('\n');
        }

        /// <summary>
        /// Cleanses the source code by replacing troublesome characters with simple characters
        /// </summary>
        private void CleanseSourceCode()
        {
            // Replace tabs with spaces
            // Replace \r\n with \n

            _sourceCodeInput = _sourceCodeInput.Replace ('\t', ' ');
            _sourceCodeInput = _sourceCodeInput.Replace ("\r\n", "\n");
        }

        /// <summary>
        /// Reads the next character for the source code file
        /// </summary>
        /// <returns>The next character from the soruce code file</returns>
        public char ReadChar()
        {
            // Read next character
            char c = _sourceCodeInput [_nextCharIndex];

            // Increment indexes
            _nextCharIndex++;
            Column++;

            // Increment line and reset column if we hit a new line character
            if (c == '\n')
            {
                Line++;
                Column = 1;
            }

            return c;
        }

        /// <summary>
        /// Reads a full line from the source code
        /// </summary>
        /// <returns>The next full line from the source code.</returns>
        public string ReadLine()
        {
            StringBuilder sb = new StringBuilder ();
            char c;

            while (CanRead && (c = ReadChar()) != '\n' )
                sb.Append (c);
            
            return sb.ToString ();
        }

        /// <summary>
        /// Indicates whether this <see cref="T:CompilerTools.Scanner.SourceCodeReader"/>
        /// can read from the source code file. Returns false when we are at the
        /// end of the file
        /// </summary>
        /// <value><c>true</c> if can read; otherwise, <c>false</c>.</value>
        public bool CanRead {
            get { return _nextCharIndex < _sourceCodeInput.Length;}
        }

        /// <summary>
        /// Rewind the reader back one character to allow reprocessing.
        /// </summary>
        public void Rewind()
        {
            _nextCharIndex--;
            Column--;

            if (_sourceCodeInput [_nextCharIndex] == '\n')
            {
                Line--;
                Column = _lines[Line - 1].Length + 1; // Add 1 because \n won't appear in array
            }
        }

    }
}

