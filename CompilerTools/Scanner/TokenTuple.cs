﻿using System;
using CompilerTools.Parser.SymbolTable;

namespace CompilerTools.Scanner
{
    /// <summary>
    /// Tuple of data that defines a Token.
    /// <typeparam name="T">
    ///     Typically, T will be an enumerated type which enumerates the possible token types
    /// </typeparam>
    /// </summary>
    public class TokenTuple<T>
    {
        public T TokenType { get; set; }
        public string Lexeme { get; set; }
        public int LineNumber { get; set; }
        public int ColumnNumber { get; set; }
        public SymbolTableRecord SymbolTableRecord { get; set; }

        public TokenTuple () : this (default(T), null, -1, -1) { }
        public TokenTuple (T type) : this (type, null, -1, -1) { }
        public TokenTuple (T type, string lexeme) : this(type, lexeme, -1, -1) { }

        public TokenTuple (T type, string lexeme, int line, int column)
        {
            this.TokenType = type;
            this.Lexeme = lexeme;
            this.LineNumber = line;
            this.ColumnNumber = column;
            this.SymbolTableRecord = null;
        }

        public override string ToString ()
        {
            return (Lexeme != null)
                ? string.Format ("{0} ({1}) ({2}, {3})", TokenType, Lexeme, LineNumber, ColumnNumber)
                : string.Format ("{0} ({1}, {2})", TokenType, LineNumber, ColumnNumber);
        }

        public override bool Equals (object obj)
        {
            if (this == obj) return true;
            if (this.GetType () != obj.GetType ()) return false;

            TokenTuple<T> token = (TokenTuple<T>)obj;

            if (!this.TokenType.Equals(token.TokenType)) return false;
            if (this.Lexeme       != token.Lexeme)       return false;
            if (this.LineNumber   != token.LineNumber)   return false;
            if (this.ColumnNumber != token.ColumnNumber) return false;

            return true;
        }

        public override int GetHashCode ()
        {
            int result = TokenType.GetHashCode ();
            result = 31 * result + (Lexeme != null ? Lexeme.GetHashCode () : 0);
            result = 31 * result + (LineNumber);
            result = 31 * result + (ColumnNumber);
            return result;
        }

    }
}

