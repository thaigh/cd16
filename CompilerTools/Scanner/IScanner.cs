﻿using System;
using System.Collections.Generic;
using CompilerTools.Scanner;

namespace CompilerTools.Scanner
{
    /// <summary>
    /// Defines the interface that allows the compiler to read a source code file
    /// using a Scanner
    /// </summary>
    public interface IScanner<T>
    {
        /// <summary>
        /// Gets all tokens within the source code file
        /// </summary>
        /// <returns>All tokens within the file</returns>
        ICollection<TokenTuple<T>> GetAllTokens ();

        /// <summary>
        /// Gets the next valid token from the scanner
        /// </summary>
        /// <returns>The next valid token.</returns>
        TokenTuple<T> GetNextValidToken ();
    }
}

