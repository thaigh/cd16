﻿using System;
namespace CompilerTools.StateMachine
{
    /// <summary>
    /// Represents a single state within the State Machine.
    /// </summary>
    public abstract class MachineState<T>
    {
        /// <summary>
        /// Holds a reference to the State Machine this state is registered with.
        /// A single state object can only be registered with one State Machine at
        /// a given time
        /// </summary>
        /// <value>The machine.</value>
        public StateMachine<T> Machine { get; set; }

        public MachineState () { }

        /// <summary>
        /// Override this to provide additional logic when a Machine State is entered
        /// </summary>
        public virtual void OnEnter () { }

        /// <summary>
        /// Override this to provide additional logic when the Machine State is exited
        /// </summary>
        public virtual void OnExit() { }

        /// <summary>
        /// Checks whether the state transition from the current state to the "newState" is valid.
        /// By default, all transitions are valid. Developers can override this to provide additional
        /// logic around state transitions
        /// </summary>
        /// <returns><c>true</c>, if state transition is valid, <c>false</c> otherwise.</returns>
        /// <param name="newState">New state to transition to.</param>
        public virtual bool ValidTransition(T newState) {
            // By default, all transitions are valid
            return true;
        }

        public virtual void Update (char c) { }
        public virtual void Update (string s) { }
        public virtual void Update (int i) { }
        public virtual void Update (float f) { }
        public virtual void Update (double d) { }
        public virtual void Update (StateMachineMessage message) { }
        public virtual void Update () { }

    }
}

