﻿using System;
namespace CompilerTools.StateMachine
{

    /// <summary>
    /// Generic abstract class that allows developers to extend and provide their
    /// own messages when updating the State Machine
    /// </summary>
    public abstract class StateMachineMessage
    {
        public StateMachineMessage () { }
    }
}

