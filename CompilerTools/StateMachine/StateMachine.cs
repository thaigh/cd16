﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CompilerTools.StateMachine
{
    /// <summary>
    /// Generic state machine
    /// </summary>
    public class StateMachine<T>
    {
        /// <summary>
        /// Mock state that does nothing. Can be used as dummy state
        /// </summary>
        /// <value>The state of the mock empty.</value>
        public static EmptyState NewEmptyState { get { return new EmptyState (); } }

        /// <summary>
        /// Empty state that does not apply any logic during the update routine
        /// </summary>
        public class EmptyState : MachineState<T>
        {
            // Do nothing
        }

        // State Machine Responsibilities:
        //  - Store collection of configurable states and enums (Dictionary / Array)
        //  - Update state when a state calls it to
        //  - Process an input, based on the current state

        // Stores the state lookup to ensure we can't go to an undefined state
        // I'm not particularly happy about using a simple Dictionary, a wrapper would look cleaner
        private IDictionary<T, MachineState<T>> _stateLookup;

        /// <summary>
        /// Gets the current state of the State Machine.
        /// </summary>
        /// <value>The current state of the State Machine.</value>
        public T CurrentState { get; private set; }

        /// <summary>
        /// Gets the previous state of the State Machine.
        /// </summary>
        /// <value>The previous state of the State Machine.</value>
        public T PreviousState { get; private set; }

        /// <summary>
        /// Gets the Global MachineState reference of the State Machine.
        /// </summary>
        /// <value>The global state of the State Machine.</value>
        public MachineState<T> GlobalStateRef  { get; private set; }

        /// <summary>
        /// Gets the Current MachineState reference of the State Machine.
        /// </summary>
        /// <value>The current state of the State Machine.</value>
        public MachineState<T> CurrentStateRef { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:CompilerTools.StateMachine.StateMachine`1"/> class.
        /// <para>
        ///     A global state can be provided to allow additional logic processing that will be
        ///     updated on each update call. Global states can hold logic that applies to all states in the
        ///     State Machine
        /// </para>
        /// </summary>
        /// <param name="stateLookup">State lookup.</param>
        /// <param name="initialState">Initial state.</param>
        /// <param name="globalState">Global state.</param>
        public StateMachine (IDictionary<T, MachineState<T>> stateLookup, T initialState, MachineState<T> globalState = null)
        {

            _stateLookup = stateLookup;
            GlobalStateRef = globalState == null ? NewEmptyState : globalState;

            CurrentStateRef = NewEmptyState; // Only for initialisation purposes
            SetState (initialState);

            // Give all states in the state lookup a reference to this state machine
            foreach (var state in _stateLookup.Values)
                state.Machine = this;
        }

        #region UpdateMethods

        /// <summary>
        /// Updates the State Machine's current state with the char input
        /// </summary>
        /// <param name="c">A character</param>
        public void Update(char c)
        {
            CurrentStateRef.Update (c);
            GlobalStateRef.Update (c);
        }

        /// <summary>
        /// Updates the State Machine's current state with the string input
        /// </summary>
        /// <param name="s">A string</param>
        public void Update (string s)
        {
            CurrentStateRef.Update (s);
            GlobalStateRef.Update (s);
        }

        /// <summary>
        /// Updates the State Machine's current state with the integer input
        /// </summary>
        /// <param name="i">An integer</param>
        public void Update (int i)
        {
            CurrentStateRef.Update (i);
            GlobalStateRef.Update (i);
        }

        /// <summary>
        /// Updates the State Machine's current state with the float input
        /// </summary>
        /// <param name="f">A float</param>
        public void Update (float f)
        {
            CurrentStateRef.Update (f);
            GlobalStateRef.Update (f);
        }

        /// <summary>
        /// Updates the State Machine's current state with the double input
        /// </summary>
        /// <param name="d">A double</param>
        public void Update (double d)
        {
            CurrentStateRef.Update (d);
            GlobalStateRef.Update (d);
        }

        /// <summary>
        /// Updates the State Machine's current state with no input
        /// </summary>
        public void Update ()
        {
            CurrentStateRef.Update ();
            GlobalStateRef.Update ();
        }

        /// <summary>
        /// Updates the State Machine's current state with a user defined message
        /// </summary>
        /// <param name="message">A message.</param>
        public void Update (StateMachineMessage message)
        {
            CurrentStateRef.Update (message);
            GlobalStateRef.Update (message);
        }

        #endregion

        /// <summary>
        /// Sets the state of the State Machine.
        /// Requires the newState to be registered with State Machine
        /// </summary>
        /// <param name="newState">New state to transition to.</param>
        public void SetState(T newState)
        {
            CheckStateRegistered (newState);

            if (CurrentStateRef.ValidTransition(newState))
            {
                UpdateState (newState);
                TransitionState (newState);    
            }

        }

        /// <summary>
        /// Checks to see if the newState has been registered with the State Machine.
        /// Acts as a precondition check, prior to state transitions
        /// </summary>
        /// <param name="newState">New state to transition to.</param>
        private void CheckStateRegistered (T newState)
        {
            if (!_stateLookup.ContainsKey (newState))
                throw new ArgumentException ("Invalid State transition. State not registered");
        }

        /// <summary>
        /// Updates the state tracking variables.
        /// </summary>
        /// <param name="newState">New state we are transitioning to.</param>
        private void UpdateState (T newState)
        {
            PreviousState = CurrentState;
            CurrentState = newState;
        }

        /// <summary>
        /// Perform the state transition.
        /// </summary>
        /// <param name="newState">New state we are transitioning to.</param>
        private void TransitionState(T newState)
        {
            CurrentStateRef.OnExit ();
            CurrentStateRef = _stateLookup [newState];
            CurrentStateRef.OnEnter ();
        }

        /// <summary>
        /// Reverse lookup of a MachineState to its key value in the State Machine translation lookup table
        /// </summary>
        /// <returns>The MachineState reference.</returns>
        /// <param name="state">The corresponding key value.</param>
        public T LookupState(MachineState<T> state)
        {
            return _stateLookup.FirstOrDefault (sl => sl.Value == state).Key;
        }

    }
}

