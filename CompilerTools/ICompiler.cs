﻿using System;
using System.Collections.Generic;
using CompilerTools.Parser;
using CompilerTools.Scanner;

namespace CompilerTools
{
    /// <summary>
    /// Defines the interface for a Compiler
    /// </summary>
    public interface ICompiler<T>
    {
        /// <summary>
        /// Runs the compilation routine. Generally involves running
        /// Lexical Analysis, Syntactic Analysis (and Semantic Analysis),
        /// and Code Generation
        /// </summary>
        void Compile ();

        /// <summary>
        /// Lexically analyses the input code module for lexically valid tokens
        /// </summary>
        /// <returns>A collection of lexically valid tokens.</returns>
        ICollection<TokenTuple<T>> LexicalAnalysis ();

        /// <summary>
        /// Syntactically analyses the generated token stream for syntaxtically
        /// valid structures.
        /// <para>Semantic Analysis is generally performed during this stage</para>
        /// </summary>
        /// <returns>A syntactically and semantically valid tree structure.</returns>
        /// <param name="tokens">A collection of lexically valid tokens.</param>
        TreeNode SyntacticAnalysis (ICollection<TokenTuple<T>> tokens);

        /// <summary>
        /// Generates the code module to be run on a target architecture
        /// </summary>
        /// <returns>The compiled code module.</returns>
        /// <param name="syntaxTree">A syntactically and semantically valid tree structure.</param>
        string CodeGeneration (TreeNode syntaxTree);
    }
}

