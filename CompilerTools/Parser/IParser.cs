﻿using System;
using System.Collections.Generic;
using CompilerTools.Scanner;

namespace CompilerTools.Parser
{
    public interface IParser<T>
    {
        TreeNode Parse (ICollection<TokenTuple<T>> tokens);
    }
}

