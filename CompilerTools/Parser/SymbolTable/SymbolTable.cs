﻿using System;
using System.Collections.Generic;

namespace CompilerTools.Parser.SymbolTable
{
    public class SymbolTable
    {
        // Responsibilities:
        //  - Store dictionary lookup of Symbols
        //  - Provide Lookup and insertion of symbols
        //  - Symbols can have different scopes

        private IDictionary<int, SymbolTableRecord> _symbolTable = new Dictionary<int, SymbolTableRecord>();

        public SymbolTable () { }

        public void Insert(string lexeme, string scope)
        {
            SymbolTableRecord rec = new SymbolTableRecord (lexeme, scope);
            Insert (rec);
        }

        public void Insert(SymbolTableRecord rec)
        {
            _symbolTable.Add (rec.Key, rec);
        }

        public SymbolTableRecord Lookup(SymbolTableRecord rec)
        {
            return Lookup(rec.Key);
        }

        public SymbolTableRecord Lookup (int key)
        {
            return _symbolTable [key];
        }

        public SymbolTableRecord Lookup (string scope, string lexeme)
        {
            int key = SymbolTableRecord.KeyHashingAlgorithm(scope, lexeme);
            return Lookup(key);
        }

        public bool Contains (SymbolTableRecord rec)
        {
            return Contains (rec.Key);
        }

        public bool Contains (int key)
        {
            return _symbolTable.Keys.Contains (key);
        }

        public bool Contains (string scope, string lexeme)
        {
            int key = SymbolTableRecord.KeyHashingAlgorithm (scope, lexeme);
            return Contains (key);
        }

    }
}

