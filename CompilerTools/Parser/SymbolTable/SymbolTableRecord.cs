﻿using System;
using System.Dynamic;

namespace CompilerTools.Parser.SymbolTable
{
    public class SymbolTableRecord
    {
        public string Lexeme { get; set; }
        public string Scope { get; set; }

        public SymbolTableMetadata Properties { get; set; }

        public int Key { get; private set; }

        public SymbolTableRecord (string lexeme, string scope)
        {
            this.Lexeme = lexeme;
            this.Scope = scope;

            this.Key = KeyHashingAlgorithm (scope, lexeme);
            this.Properties = new SymbolTableMetadata ();
        }

        // I want to make the Symbol table more flexible by allowing users to
        // define their own hashing algorithm. I can't figure out how this is going
        // to work just yet. Perhaps I'm focussing too much on how to customise the code.
        // Maybe a Factory pattern would help here?

        public static int KeyHashingAlgorithm(string lexeme, string scope)
        {
            return (scope + lexeme).GetHashCode ();
        }

        public override bool Equals (object obj)
        {
            if (this == obj) return true;
            if (this.GetType () != obj.GetType ()) return false;

            SymbolTableRecord sym = (SymbolTableRecord)obj;

            if (this.Key != sym.Key) return false;
            if (this.Lexeme != sym.Lexeme) return false;
            if (this.Scope != sym.Scope) return false;
            if (this.Properties != null
                ? !this.Properties.Equals (sym.Properties)
                : sym.Properties != null) return false;

            return true;
        }

        public override int GetHashCode ()
        {
            int result = Key;
            result = 31 * result + (Lexeme != null ? Lexeme.GetHashCode () : 0);
            result = 31 * result + (Scope != null ? Scope.GetHashCode () : 0);
            result = 31 * result + (Properties != null ? Properties.GetHashCode () : 0);
            return result;
        }

    }
}

