﻿using System;
namespace CompilerTools.Parser.SymbolTable
{
    public class SymbolTableMetadata
    {
        public SymbolTableMetadata () { }

        public override bool Equals (object obj)
        {
            return object.ReferenceEquals (this, obj);
        }

        public override int GetHashCode ()
        {
            return base.GetHashCode();
        }
    }
}

