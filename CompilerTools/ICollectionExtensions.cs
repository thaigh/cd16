﻿using System;
using System.Linq;

namespace System.Collections.Generic
{
    public static class ICollectionExtensions
    {
        
        /// <summary>
        /// Extension method to remove all elements from an ICollection where elements match
        /// a predicate. Taken from http://stackoverflow.com/a/653602/2442468
        /// </summary>
        /// <returns><c>true</c>, if at least one element was removed, <c>false</c> otherwise.</returns>
        /// <param name="collection">Collection of elements</param>
        /// <param name="predicate">Predicate for removal</param>
        /// <typeparam name="T">The type parameter of the Collection.</typeparam>
        public static bool RemoveAll<T> (this ICollection<T> collection, Func<T, bool> predicate)
        {
            T element;
            bool removed = false;

            for (int i = 0; i < collection.Count; i++) {
                element = collection.ElementAt (i);
                if (predicate (element)) {
                    collection.Remove (element);
                    i--;
                    removed = true;
                }
            }

            return removed;
        }

    }
}

