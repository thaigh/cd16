﻿using System;
using System.Collections.Generic;
using CD16.Scanner;
using NUnit.Framework;
using TokenTuple = CompilerTools.Scanner.TokenTuple<CD16.Scanner.CD16Token>;

namespace CD16.Tests.Scanner
{
    [TestFixture]
    public class InvalidCharTests
    {
        [Test]
        public void InvalidToken_Hash ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner ("#");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.Undefined, "#", 1, 1),
                new TokenTuple(CD16Token.EndOfFile, null, 1, 2)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

        [Test]
        public void InvalidToken_Hash_WithIdentifierFirst ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner ("apple#");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.Identifier, "apple", 1, 1),
                new TokenTuple(CD16Token.Undefined, "#", 1, 6),
                new TokenTuple(CD16Token.EndOfFile, null, 1, 7)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

        [Test]
        public void InvalidToken_Hash_WithIdentifierSecond ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner ("#apple");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.Undefined, "#", 1, 1),
                new TokenTuple(CD16Token.Identifier, "apple", 1, 2),
                new TokenTuple(CD16Token.EndOfFile, null, 1, 7)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }
    }
}

