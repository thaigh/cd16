﻿using System;
using System.Collections.Generic;
using CD16.Scanner;
using NUnit.Framework;
using TokenTuple = CompilerTools.Scanner.TokenTuple<CD16.Scanner.CD16Token>;

namespace CD16.Tests.Scanner
{
    [TestFixture]
    public class IntLiteralTests
    {
        [Test ()]
        public void Integer_SimpleInt_123 ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner ("123");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.IntegerLiteral, "123", 1, 1),
                new TokenTuple(CD16Token.EndOfFile, null, 1, 4)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

        [Test ()]
        public void Integer_IntWithWhitespace_123 ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner (" 123 ");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.IntegerLiteral, "123", 1, 2),
                new TokenTuple(CD16Token.EndOfFile, null, 1, 6)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

        [Test ()]
        public void Integer_IntAndIdentifier ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner (" 123abc ");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.IntegerLiteral, "123", 1, 2),
                new TokenTuple(CD16Token.Identifier, "abc", 1, 5),
                new TokenTuple(CD16Token.EndOfFile, null, 1, 9)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

        [Test ()]
        public void Integer_IdentifierAndInt ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner (" abc123 ");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.Identifier, "abc123", 1, 2),
                new TokenTuple(CD16Token.EndOfFile, null, 1, 9)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

        [Test ()]
        public void Integer_IntSemi ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner (" 123; ");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.IntegerLiteral, "123", 1, 2),
                new TokenTuple(CD16Token.SemiColon, null, 1, 5),
                new TokenTuple(CD16Token.EndOfFile, null, 1, 7)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }


        [Test ()]
        public void Integer_FailedFloat ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner (" 123. ");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.Undefined, "123.", 1, 2),
                new TokenTuple(CD16Token.EndOfFile, null, 1, 7)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

        [Test ()]
        public void Integer_SimpleFloat ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner ("123.456");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.FloatLiteral, "123.456", 1, 1),
                new TokenTuple(CD16Token.EndOfFile, null, 1, 8)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

        [Test ()]
        public void Integer_SimpleFloat_WithWhitespaces ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner (" 123.456 ");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.FloatLiteral, "123.456", 1, 2),
                new TokenTuple(CD16Token.EndOfFile, null, 1, 10)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

        [Test ()]
        public void Float_FloatAndIdentifier ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner ("123.456abc");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.FloatLiteral, "123.456", 1, 1),
                new TokenTuple(CD16Token.Identifier, "abc", 1, 8),
                new TokenTuple(CD16Token.EndOfFile, null, 1, 11)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

        [Test ()]
        public void Float_IdentifierAndFloat ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner ("abc123.456");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.Identifier, "abc123", 1, 1),
                new TokenTuple(CD16Token.Dot, null, 1, 7),
                new TokenTuple(CD16Token.IntegerLiteral, "456", 1, 8),
                new TokenTuple(CD16Token.EndOfFile, null, 1, 11)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

        [Test ()]
        public void Float_FloatAndSemi ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner ("123.456;");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.FloatLiteral, "123.456", 1, 1),
                new TokenTuple(CD16Token.SemiColon, null, 1, 8),
                new TokenTuple(CD16Token.EndOfFile, null, 1, 9)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

        [Test ()]
        public void Float_Float_EndsWithNewLine ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner ("123.456\n");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.FloatLiteral, "123.456", 1, 1),
                new TokenTuple(CD16Token.EndOfFile, null, 2, 1)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

        [Test ()]
        public void Integer_Integer_EndsWithNewLine ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner ("123\n");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.IntegerLiteral, "123", 1, 1),
                new TokenTuple(CD16Token.EndOfFile, null, 2, 1)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

    }
}

