﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using CD16.Scanner;
using TokenTuple = CompilerTools.Scanner.TokenTuple<CD16.Scanner.CD16Token>;

namespace CD16.Tests.Scanner
{
    [TestFixture ()]
    public class IdentifierTests
    {
        [Test ()]
        public void Identifier_Begin_Lower_Keyword ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner ("begin");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.Begin, null, 1, 1),
                new TokenTuple(CD16Token.EndOfFile, null, 1, 6)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

        [Test ()]
        public void Identifier_Begin_Upper_Keyword ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner (" BEGIN");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.Begin, null, 1, 2),
                new TokenTuple(CD16Token.EndOfFile, null, 1, 7)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

        [Test ()]
        public void Identifier_Begin_Mixed_Keyword ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner ("BeGiN ");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.Begin, null, 1, 1),
                new TokenTuple(CD16Token.EndOfFile, null, 1, 7)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

        [Test ()]
        public void Identifier_BeginX_Identifier ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner (" beginx ");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.Identifier, "beginx", 1, 2),
                new TokenTuple(CD16Token.EndOfFile, null, 1, 9)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

        [Test ()]
        public void Identifier_Begin_MultiIdentifier ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner (" is beginx \n cd16 HeLLo ");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.Is, null, 1, 2),
                new TokenTuple(CD16Token.Identifier, "beginx", 1, 5),
                new TokenTuple(CD16Token.CD16, null, 2, 2),
                new TokenTuple(CD16Token.Identifier, "HeLLo", 2, 7),
                new TokenTuple(CD16Token.EndOfFile, null, 2, 13)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

        [Test ()]
        public void Identifier_MixedWithPunctuation ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner (" is beginx \n!apple;\n cd16 HeLLo ");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.Is, null, 1, 2),
                new TokenTuple(CD16Token.Identifier, "beginx", 1, 5),
                new TokenTuple(CD16Token.Exclamation, null, 2, 1),
                new TokenTuple(CD16Token.Identifier, "apple", 2, 2),
                new TokenTuple(CD16Token.SemiColon, null, 2, 7),
                new TokenTuple(CD16Token.CD16, null, 3, 2),
                new TokenTuple(CD16Token.Identifier, "HeLLo", 3, 7),
                new TokenTuple(CD16Token.EndOfFile, null, 3, 13)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

        [Test ()]
        public void Identifier_BeginX_EndsWithNewLine ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner ("beginx\n");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.Identifier, "beginx", 1, 1),
                new TokenTuple(CD16Token.EndOfFile, null, 2, 1)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

        [Test ()]
        public void Identifier_KeywordBegin_EndsWithNewLine ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner ("begin\n");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.Begin, null, 1, 1),
                new TokenTuple(CD16Token.EndOfFile, null, 2, 1)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

    }
}

