﻿using System;
using System.Collections.Generic;
using CD16.Scanner;
using NUnit.Framework;
using TokenTuple = CompilerTools.Scanner.TokenTuple<CD16.Scanner.CD16Token>;

namespace CD16.Tests.Scanner
{
    [TestFixture]
    public class StringLiteralTests
    {
        [Test]
        public void String_SimpleString ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner ("\"This is a String\"");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.StringLiteral, "This is a String", 1, 1),
                new TokenTuple(CD16Token.EndOfFile, null, 1, 19)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

        [Test]
        public void String_SimpleString_WithWhitespace ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner (" \"This is a String\" ");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.StringLiteral, "This is a String", 1, 2),
                new TokenTuple(CD16Token.EndOfFile, null, 1, 21)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

        [Test]
        public void String_FailedString_NewLine ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner (" \"This is \n a String\" ");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.Undefined, "This is ", 1, 2),
                new TokenTuple(CD16Token.Identifier, "a", 2, 2),
                new TokenTuple(CD16Token.Identifier, "String", 2, 4),
                new TokenTuple(CD16Token.Undefined, " ", 2, 10),
                new TokenTuple(CD16Token.EndOfFile, null, 2, 12)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

        [Test]
        public void String_StringSurroundedBySemis ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner (";\"This is a String\";");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.SemiColon, null, 1, 1),
                new TokenTuple(CD16Token.StringLiteral, "This is a String", 1, 2),
                new TokenTuple(CD16Token.SemiColon, null, 1, 20),
                new TokenTuple(CD16Token.EndOfFile, null, 1, 21)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

        [Test]
        public void String_StringSurroundedByIdentifiers ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner ("abc\"This is a String\"def");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.Identifier, "abc", 1, 1),
                new TokenTuple(CD16Token.StringLiteral, "This is a String", 1, 4),
                new TokenTuple(CD16Token.Identifier, "def", 1, 22),
                new TokenTuple(CD16Token.EndOfFile, null, 1, 25)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

        [Test]
        public void String_StringSurroundedByInts ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner ("123\"This is a String\"456");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.IntegerLiteral, "123", 1, 1),
                new TokenTuple(CD16Token.StringLiteral, "This is a String", 1, 4),
                new TokenTuple(CD16Token.IntegerLiteral, "456", 1, 22),
                new TokenTuple(CD16Token.EndOfFile, null, 1, 25)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

        [Test]
        public void String_StringSurroundedByFloats ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner ("1.3\"This is a String\"4.6");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.FloatLiteral, "1.3", 1, 1),
                new TokenTuple(CD16Token.StringLiteral, "This is a String", 1, 4),
                new TokenTuple(CD16Token.FloatLiteral, "4.6", 1, 22),
                new TokenTuple(CD16Token.EndOfFile, null, 1, 25)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

        [Test]
        public void String_SingleCharString ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner ("\"-\"-");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.StringLiteral, "-", 1, 1),
                new TokenTuple(CD16Token.Minus, null, 1, 4),
                new TokenTuple(CD16Token.EndOfFile, null, 1, 5)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

        [Test]
        public void String_KeywordAsString ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner ("not\"begin\"cd16");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.Not, null, 1, 1),
                new TokenTuple(CD16Token.StringLiteral, "begin", 1, 4),
                new TokenTuple(CD16Token.CD16, null, 1, 11),
                new TokenTuple(CD16Token.EndOfFile, null, 1, 15)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

        [Test]
        public void String_String_EndsWithNewLine ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner ("\"begin\"\n");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.StringLiteral, "begin", 1, 1),
                new TokenTuple(CD16Token.EndOfFile, null, 2, 1)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

    }
}

