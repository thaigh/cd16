﻿using System;
using System.Collections.Generic;
using CD16.Scanner;
using NUnit.Framework;
using TokenTuple = CompilerTools.Scanner.TokenTuple<CD16.Scanner.CD16Token>;

namespace CD16.Tests.Scanner
{
    [TestFixture]
    public class CommentTests
    {
        [Test ()]
        public void Comment_CommentAfterKeyword ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner ("begin /-- This is a comment");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.Begin, null, 1, 1),
                new TokenTuple(CD16Token.EndOfFile, null, 1, 28)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

        [Test ()]
        public void Comment_CommentOnly_One ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner ("/-- This is a comment begin");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.EndOfFile, null, 1, 28)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

        [Test ()]
        public void Comment_CommentOnly_Two ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner ("/-- Comment 1\n/-- Comment2");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.EndOfFile, null, 2, 13)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

        [Test ()]
        public void Comment_Mixed_CommentsAndKewords ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner ("/-- Comment 1\nbegin cd16/-- Comment2\n;/-- Comment3\n");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.Begin, null, 2, 1),
                new TokenTuple(CD16Token.CD16, null, 2, 7),
                new TokenTuple(CD16Token.SemiColon, null, 3, 1),
                new TokenTuple(CD16Token.EndOfFile, null, 4, 1)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

        [Test ()]
        public void Comment_FailedComment_ReturnUndefinedToken ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner ("/- NotAComment");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.Undefined, "/-", 1, 1),
                new TokenTuple(CD16Token.Identifier, "NotAComment", 1, 4),
                new TokenTuple(CD16Token.EndOfFile, null, 1, 15)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }
    }
}

