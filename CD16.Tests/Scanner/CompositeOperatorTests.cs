﻿using System;
using System.Collections.Generic;
using CD16.Scanner;
using NUnit.Framework;
using TokenTuple = CompilerTools.Scanner.TokenTuple<CD16.Scanner.CD16Token>;

namespace CD16.Tests.Scanner
{
    [TestFixture ()]
    public class CompositeOperatorTests
    {
        [Test ()]
        public void CompositeOp_LessLess ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner ("<<");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.LessLess, null, 1, 1),
                new TokenTuple(CD16Token.EndOfFile, null, 1, 3)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

        [Test ()]
        public void CompositeOp_GreaterGreater_X2 ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner (" >> >> ");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.GreaterGreater, null, 1, 2),
                new TokenTuple(CD16Token.GreaterGreater, null, 1, 5),
                new TokenTuple(CD16Token.EndOfFile, null, 1, 8)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

        [Test ()]
        public void CompositeOp_DoubleEquals_X2 ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner ("====");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.DoubleEquals, null, 1, 1),
                new TokenTuple(CD16Token.DoubleEquals, null, 1, 3),
                new TokenTuple(CD16Token.EndOfFile, null, 1, 5)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }


        [Test ()]
        public void CompositeOp_Bang_Mixed ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner ("! !< !a;");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.Exclamation, null, 1, 1),
                new TokenTuple(CD16Token.Exclamation, null, 1, 3),
                new TokenTuple(CD16Token.LessThan,  null, 1, 4),
                new TokenTuple(CD16Token.Exclamation, null, 1, 6),
                new TokenTuple(CD16Token.Identifier, "a", 1, 7),
                new TokenTuple(CD16Token.SemiColon, null, 1, 8),
                new TokenTuple(CD16Token.EndOfFile, null, 1, 9)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

        [Test]
        public void CompositeOp_LessLess_EndsWithNewLine ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner ("<<\n");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.LessLess, null, 1, 1),
                new TokenTuple(CD16Token.EndOfFile, null, 2, 1)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

    }
}

