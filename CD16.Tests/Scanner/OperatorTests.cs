﻿using System;
using System.Collections.Generic;
using CD16.Scanner;
using NUnit.Framework;
using TokenTuple = CompilerTools.Scanner.TokenTuple<CD16.Scanner.CD16Token>;

namespace CD16.Tests.Scanner
{
    [TestFixture ()]
    public class OperatorTests
    {
        [Test ()]
        public void Operator_SemiColon_Singular ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner (";");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.SemiColon, null, 1, 1),
                new TokenTuple(CD16Token.EndOfFile, null, 1, 2)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

        [Test ()]
        public void Operator_LeftBracket_Double ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner (" [[ ");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.LeftBracket, null, 1, 2),
                new TokenTuple(CD16Token.LeftBracket, null, 1, 3),
                new TokenTuple(CD16Token.EndOfFile, null, 1, 5)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

        [Test ()]
        public void Operator_Mixed_SeparatedByTab ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner (" :\t] ");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.Colon, null, 1, 2),
                new TokenTuple(CD16Token.RightBracket, null, 1, 4),
                new TokenTuple(CD16Token.EndOfFile, null, 1, 6)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

        [Test ()]
        public void Operator_Mixed_SeparatedByWindowsNewLine ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner (" .\r\n, ");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.Dot, null, 1, 2),
                new TokenTuple(CD16Token.Comma, null, 2, 1),
                new TokenTuple(CD16Token.EndOfFile, null, 2, 3)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

        [Test ()]
        public void Operator_MixedWithIdentifiersAndCompositeOps ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner (".a<<b;");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.Dot, null, 1, 1),
                new TokenTuple(CD16Token.Identifier, "a", 1, 2),
                new TokenTuple(CD16Token.LessLess, null, 1, 3),
                new TokenTuple(CD16Token.Identifier, "b", 1, 5),
                new TokenTuple(CD16Token.SemiColon, null, 1, 6),
                new TokenTuple(CD16Token.EndOfFile, null, 1, 7)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }

        [Test]
        public void Operator_Semi_EndsWithNewLine ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner (";\n");
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.SemiColon, null, 1, 1),
                new TokenTuple(CD16Token.EndOfFile, null, 2, 1)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }
    }
}

