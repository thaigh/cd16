﻿using System;
using System.Collections.Generic;
using CD16.Scanner;
using NUnit.Framework;
using TokenTuple = CompilerTools.Scanner.TokenTuple<CD16.Scanner.CD16Token>;

namespace CD16.Tests.Scanner
{
    [TestFixture]
    public class ProgramTests
    {
        [Test]
        public void Identifier_MixedWithPunctuation ()
        {
            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner (
@"begin cd16 myprog

  /-- Prints hello world!
  Out << ""Hello World""

end cd16 myprog
");
            
            ICollection<TokenTuple> actualTokens = scn.GetAllTokens ();

            ICollection<TokenTuple> expectedTokens = new List<TokenTuple>
            {
                new TokenTuple(CD16Token.Begin, null, 1, 1),
                new TokenTuple(CD16Token.CD16, null, 1, 7),
                new TokenTuple(CD16Token.Identifier, "myprog", 1, 12),
                new TokenTuple(CD16Token.Out, null, 4, 3),
                new TokenTuple(CD16Token.LessLess, null, 4, 7),
                new TokenTuple(CD16Token.StringLiteral, "Hello World", 4, 10),
                new TokenTuple(CD16Token.End, null, 6, 1),
                new TokenTuple(CD16Token.CD16, null, 6, 5),
                new TokenTuple(CD16Token.Identifier, "myprog", 6, 10),
                new TokenTuple(CD16Token.EndOfFile, null, 7, 1)
            };

            Assert.AreEqual (expectedTokens.Count, actualTokens.Count);
            CollectionAssert.AreEqual (expectedTokens, actualTokens);
        }
    }
}

