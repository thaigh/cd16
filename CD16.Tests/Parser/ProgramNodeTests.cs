﻿using System;
using System.Collections.Generic;
using CD16.Parser;
using CD16.Parser.Nodes;
using CD16.Parser.Nodes.Globals;
using CD16.Scanner;
using CompilerTools.Parser;
using CompilerTools.Parser.SymbolTable;
using NUnit.Framework;
using TokenTuple = CompilerTools.Scanner.TokenTuple<CD16.Scanner.CD16Token>;

namespace CD16.Tests.Parser
{
    [TestFixture]
    public class ProgramNodeTests
    {
        [Test]
        public void ProgramNodeTests_TestProg ()
        {
            string program = @"
cd16 hello
/--main
/--    out << ""Hello Karen"" << line;
/--end
cd16 hello";

            CD16.Scanner.Scanner scn = new CD16.Scanner.Scanner (program);
            ICollection<TokenTuple> tokens = scn.GetAllTokens ();

            CD16.Parser.Parser parser = new CD16.Parser.Parser ();
            TreeNode actualNode = parser.Parse (tokens);

            TreeNode expectedProgram = new ProgramNode {
                NodeType = TreeNodeType.Program,
                DataType = NodeDataType.Program,
                IdentifierRecord = new SymbolTableRecord ("hello", "cd16") {
                    Properties = new CD16SymbolMeta { TokenType = CD16Token.Identifier } },
                LeftChild = new GlobalsNode { NodeType = TreeNodeType.Globals },
                MiddleChild = new FunctionsNode { NodeType = TreeNodeType.FuncsList },
                RightChild = new MainBodyNode { NodeType = TreeNodeType.MainBody }
            };

            Assert.AreEqual (actualNode, expectedProgram);

        }
    }
}

