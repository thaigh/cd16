﻿using System;
using System.Collections.Generic;
using CompilerTools.Scanner;
using TokenTuple = CompilerTools.Scanner.TokenTuple<CD16.Scanner.CD16Token>;

namespace CD16.Scanner
{
    public static class Tokeniser
    {
 
        public static TokenTuple GenerateToken(string lexeme)
        {
            string lower = lexeme.ToLower ();

            return (KeywordLookup.Keys.Contains (lower))
                 ? new TokenTuple (KeywordLookup [lower])
                 : new TokenTuple (CD16Token.Identifier, lexeme);
        }

        public static TokenTuple GenerateToken(int lexeme)
        {
            return new TokenTuple (CD16Token.IntegerLiteral, lexeme.ToString ());
        }

        public static TokenTuple GenerateToken(float lexeme)
        {
            return new TokenTuple (CD16Token.FloatLiteral, lexeme.ToString ());
        }

        public static TokenTuple GenerateStringToken (string lexeme)
        {
            return new TokenTuple (CD16Token.StringLiteral, lexeme);
        }

        private static IDictionary<string, CD16Token> KeywordLookup = new Dictionary<string, CD16Token>
        {
            // Keywords
            { "cd16", CD16Token.CD16 },
            { "constants", CD16Token.Constants },
            { "types", CD16Token.Types },
            { "is", CD16Token.Is },
            { "arrays", CD16Token.Arrays },
            { "main", CD16Token.Main },
            { "begin", CD16Token.Begin },
            { "end", CD16Token.End },
            { "array", CD16Token.Array },
            { "of", CD16Token.Of },
            { "func", CD16Token.Func },
            { "void", CD16Token.Void },
            { "const", CD16Token.Const },
            { "integer", CD16Token.Integer },
            { "real", CD16Token.Real },
            { "boolean", CD16Token.Boolean },
            { "for", CD16Token.For },
            { "repeat", CD16Token.Repeat },
            { "until", CD16Token.Until },
            { "if", CD16Token.If },
            { "else", CD16Token.Else },
            { "in", CD16Token.In },
            { "out", CD16Token.Out },
            { "line", CD16Token.Line },
            { "return", CD16Token.Return },
            { "not", CD16Token.Not },
            { "and", CD16Token.And },
            { "or", CD16Token.Or },
            { "xor", CD16Token.Xor },
            { "true", CD16Token.True },
            { "false", CD16Token.False },

            // Operators
            { ";", CD16Token.SemiColon },
            { "[", CD16Token.LeftBracket },
            { "]", CD16Token.RightBracket },
            { ",", CD16Token.Comma },
            { "(", CD16Token.LeftParenthesis },
            { ")", CD16Token.RightParenthesis },
            { "=", CD16Token.Equals },
            { "+", CD16Token.Plus },
            { "-", CD16Token.Minus },
            { "*", CD16Token.Star },
            { "/", CD16Token.Slash },
            { "%", CD16Token.Percent },
            { "^", CD16Token.Carat },
            { "<", CD16Token.LessThan },
            { ">", CD16Token.GreaterThan },
            { "!", CD16Token.Exclamation },
            { "\"", CD16Token.Quote },
            { ":", CD16Token.Colon },
            { ".", CD16Token.Dot },
        
            // Composite Operators
            { "<<", CD16Token.LessLess },
            { ">>", CD16Token.GreaterGreater },
            { "<=", CD16Token.LessThanEqual },
            { ">=", CD16Token.GreaterThanEqual },
            { "!=", CD16Token.NotEquals },
            { "==", CD16Token.DoubleEquals }
        };

    }
}

