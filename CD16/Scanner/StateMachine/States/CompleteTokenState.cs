﻿using System;
using CompilerTools.StateMachine;

namespace CD16.Scanner.StateMachine.States
{
    public class CompleteTokenState : MachineState<ScannerState>
    {
        public CompleteTokenState () { }

        public override void Update (char c)
        {
            // Not meant to do anytihing
        }
    }
}

