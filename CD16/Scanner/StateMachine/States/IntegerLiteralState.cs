﻿using System;
using CompilerTools.Scanner;
using CompilerTools.StateMachine;

namespace CD16.Scanner.StateMachine.States
{
    public class IntegerLiteralState : MachineState<ScannerState>
    {
        // When we are in the IntegerLiteral state:
        //  - If input is whitespace, return token
        //  - If input is char, update state to Halt
        //  - If input is numeric, consume
        //  - If input is punctuation, update state to Halt, or PossibleFloatLiteral

        public IntegerLiteralState () { }

        public override void Update (char c)
        {
            Classification _class = InputClassifier.Classify (c);

            switch (_class) {
                case Classification.Whitespace: Machine.SetState (ScannerState.CompleteToken); break;
                case Classification.Numeric:   /* Do nothing, but consume */ break;
                case Classification.Punctuation:
                {
                    Machine.SetState (c == '.' ? ScannerState.PossibleFloat : ScannerState.Halt);
                    break;
                }
                default: Machine.SetState (ScannerState.Halt); break;
            }
        }
    }
}

