﻿using System;
using CompilerTools.Scanner;
using CompilerTools.StateMachine;

namespace CD16.Scanner.StateMachine.States
{
    public class PossibleCommentState : MachineState<ScannerState>
    {
        // We are in this state when we have seen:
        //  - A '/'  character
        //  - A '/-' character combination, and looking for a final '-'
        //
        // When we are in the Possible Comment state:
        //  - If input is Punctuation, update state to Comment, or stay here
        //  - Else, update state to Halt

        private char _lastSeenChar;

        public override void OnEnter() { _lastSeenChar = '/'; }
        public override void OnExit()  { _lastSeenChar = (char)0; }

        public PossibleCommentState () { }

        public override void Update (char c)
        {
            Classification _class = InputClassifier.Classify (c);

            switch(_class)

            {
                case Classification.Punctuation:
                {
                    switch(c)
                    {
                        case '-': if (_lastSeenChar == '-') Machine.SetState (ScannerState.Comment);
                                  else _lastSeenChar = c;
                                  break;
                        default: Machine.SetState (ScannerState.Halt); break;
                    }
                    break; // End switch(c)
                }
                default: Machine.SetState (ScannerState.Halt); break;
            }
        }

    }
}

