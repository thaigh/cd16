﻿using System;
using CompilerTools.StateMachine;

namespace CD16.Scanner.StateMachine.States
{
    public class TerminatedStringState : MachineState<ScannerState>
    {
        public TerminatedStringState () { }

        public override void Update (char c)
        {
            // Not meant to do anytihing
        }
    }
}

