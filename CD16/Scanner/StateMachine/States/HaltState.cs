﻿using System;
using CompilerTools.StateMachine;

namespace CD16.Scanner.StateMachine.States
{
    public class HaltState : MachineState<ScannerState>
    {
        public HaltState () { }

        public override void Update (char c)
        {
            // Not meant to do anytihing
        }
    }
}

