﻿using System;
using CompilerTools.Scanner;
using CompilerTools.StateMachine;

namespace CD16.Scanner.StateMachine.States
{
    public class PossibleCompositeOperatorState : MachineState<ScannerState>
    {
        // If we are in this state, then the lexeme buffer should have
        // either a <, >, !, = character in the buffer
        //
        // When we are in the initialised state:
        //  - If input is not punctuation, either return comlete token, or go to invalid
        //  - If input is punctuation, update return go to CompositeOperator, or Halt

        public PossibleCompositeOperatorState () { }

        public char StagingCharacter { get; set; }

        public override void OnEnter ()
        {
            StagingCharacter = (char)0;
        }

        public override void OnExit ()
        {
            StagingCharacter = (char)0;
        }

        public override void Update (char c)
        {
            Classification _class = InputClassifier.Classify (c);

            switch (_class) {
                case Classification.Punctuation: {
                    switch (c) {
                        case '<': Machine.SetState (StagingCharacter == '<' ? ScannerState.CompositeOperator : ScannerState.Halt); break;
                        case '>': Machine.SetState (StagingCharacter == '>' ? ScannerState.CompositeOperator : ScannerState.Halt); break;
                        case '=':
                        {
                            // Work out if it is a composite operator, or two separate tokens
                            switch (StagingCharacter)
                            {
                                case '<':
                                case '>':
                                case '!':
                                case '=': Machine.SetState (ScannerState.CompositeOperator); break;
                                default:  Machine.SetState (ScannerState.Halt); break;
                            }
                            break; // End switch (StagingCharacter)
                        }
                       default: Machine.SetState (ScannerState.Operator); break;
                    }
                    break; // End switch (c)
                }
                default:
                {
                    // Character was not punctuation.
                    // Check staging character. If we are in the BangStaging state, then error
                    //Machine.SetState (StagingCharacter == '!' ? ScannerState.Invalid : ScannerState.Halt);
                    Machine.SetState (ScannerState.Halt);
                    break;
                }
            }
        }
    }
}

