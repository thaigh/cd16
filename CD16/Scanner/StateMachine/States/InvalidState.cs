﻿using System;
using CompilerTools.StateMachine;

namespace CD16.Scanner.StateMachine.States
{
    public class InvalidState : MachineState<ScannerState>
    {
        public InvalidState () { }

        public override void Update (char c)
        {
            // Not meant to do anytihing
        }
    }
}

