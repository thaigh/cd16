﻿using System;
using CompilerTools.StateMachine;

namespace CD16.Scanner.StateMachine.States
{
    public class StringLiteralState : MachineState<ScannerState>
    {
        public StringLiteralState () { }

        public override void Update (char c)
        {
            // Consume all characters until end of line character, or " character
            if (c == '\"') Machine.SetState (ScannerState.TerminatedString);
            if (c == '\n') Machine.SetState (ScannerState.Halt); // Or Invalid?
        }
    }
}

