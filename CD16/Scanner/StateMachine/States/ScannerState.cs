﻿using System;
namespace CD16.Scanner.StateMachine.States
{
    public enum ScannerState
    {
        Initialised,
        Invalid,
        CompleteToken,

        PossibleComment,
        Comment,

        StringLiteral,
        TerminatedString,

        Identifier,

        IntegerLiteral,
        PossibleFloat,
        FloatLiteral,

        Operator,
        PossibleCompositeOperator,
        CompositeOperator,

        Halt,
        EndOfFile
    }
}

