﻿using System;
using CompilerTools.Scanner;
using CompilerTools.StateMachine;

namespace CD16.Scanner.StateMachine.States
{
    public class InitialisedState : MachineState<ScannerState>
    {
        // When we are in the initialised state:
        //  - Consume whitespace
        //  - If input is char, update state to Identifier
        //  - If input is numeric, update state to Integer
        //  - If input is punctuation, update state to Operator
        //  - If input is ", update state to String Literal

        public InitialisedState () { }

        public override void Update (char c)
        {
            Classification _class = InputClassifier.Classify (c);

            switch(_class)
            {
                case Classification.Whitespace: /* Do nothing, but consume */ break;
                case Classification.Character:  Machine.SetState (ScannerState.Identifier); break;
                case Classification.Numeric:    Machine.SetState (ScannerState.IntegerLiteral); break;
                case Classification.Punctuation:
                {
                    if (!Scanner.ValidPunctuation.Contains(c))
                    {
                        Machine.SetState (ScannerState.Invalid); break; // Should break out of switch(_class)
                    }

                    switch (c)
                    {
                        case '\"': Machine.SetState (ScannerState.StringLiteral); break;
                        case '/':  Machine.SetState (ScannerState.PossibleComment); break;
                        case '<':
                        case '>':
                        case '!':
                        case '=':
                            Machine.SetState (ScannerState.PossibleCompositeOperator);
                            ((PossibleCompositeOperatorState)Machine.CurrentStateRef).StagingCharacter = c;
                            break;
                        default:   Machine.SetState (ScannerState.Operator); break;
                    }
                    break; // End switch (c)
                }
                default: Machine.SetState (ScannerState.Invalid); break;
            }
        }

    }
}

