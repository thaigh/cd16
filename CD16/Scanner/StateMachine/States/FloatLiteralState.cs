﻿using System;
using CompilerTools.Scanner;
using CompilerTools.StateMachine;

namespace CD16.Scanner.StateMachine.States
{
    public class FloatLiteralState : MachineState<ScannerState>
    {
        // When we are in the FloatLiteral state:
        //  - If input is whitespace, return token
        //  - If input is char, update state to Halt
        //  - If input is numeric, consume
        //  - If input is punctuation, update state to Halt

        public FloatLiteralState () { }

        public override void Update (char c)
        {
            Classification _class = InputClassifier.Classify (c);

            switch (_class) {
                case Classification.Whitespace: Machine.SetState (ScannerState.CompleteToken); break;
                case Classification.Numeric:   /* Do nothing, but consume */ break;
                default: Machine.SetState (ScannerState.Halt); break;
            }
        }
    }
}

