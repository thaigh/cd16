﻿using System;
using CompilerTools.StateMachine;

namespace CD16.Scanner.StateMachine.States
{
    public class EndOfFileState : MachineState<ScannerState>
    {
        public EndOfFileState () { }

        public override void Update (char c)
        {
            // Not meant to do anytihing
        }
    }
}

