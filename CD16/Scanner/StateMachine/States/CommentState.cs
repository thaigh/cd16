﻿using System;
using CompilerTools.StateMachine;

namespace CD16.Scanner.StateMachine.States
{
    public class CommentState : MachineState<ScannerState>
    {
        public CommentState () { }

        public override void Update (char c)
        {
            // Consume all characters until end of line character
            if (c == '\n') Machine.SetState (ScannerState.CompleteToken);
        }
    }
}

