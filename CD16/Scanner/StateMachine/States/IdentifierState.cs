﻿using System;
using CompilerTools.Scanner;
using CompilerTools.StateMachine;

namespace CD16.Scanner.StateMachine.States
{
    public class IdentifierState : MachineState<ScannerState>
    {
        // When we are in the Identifier state:
        //  - If input is whitespace, return token
        //  - If input is char, consume
        //  - If input is numeric, consume
        //  - If input is punctuation, update state to Halt - We have found a token
        //  - Else, update state to Halt - We have found a token

        public IdentifierState () { }

        public override void Update (char c)
        {
            Classification _class = InputClassifier.Classify (c);

            switch (_class) {
                case Classification.Whitespace: Machine.SetState (ScannerState.CompleteToken); break;
                case Classification.Character: /* Do nothing, but consume */ break;
                case Classification.Numeric:   /* Do nothing, but consume */ break;
                case Classification.Punctuation: Machine.SetState (ScannerState.Halt); break;
                default: Machine.SetState (ScannerState.Halt); break;
            }
        }
    }
}

