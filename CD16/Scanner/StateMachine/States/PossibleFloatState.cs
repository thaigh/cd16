﻿using System;
using CompilerTools.Scanner;
using CompilerTools.StateMachine;

namespace CD16.Scanner.StateMachine.States
{
    public class PossibleFloatState : MachineState<ScannerState>
    {
        // We are in this state, when we were in the IntegerLiteral State
        // and have now processed a dot (.)
        //
        // When we are in the PossibleFloat state:
        //  - If input is numeric, update state to FloatLiteral
        //  - Else, update state to Halt. This will trigger an invalid character

        public PossibleFloatState () { }

        public override void Update (char c)
        {
            Classification _class = InputClassifier.Classify (c);

            switch (_class) {
                case Classification.Numeric: Machine.SetState (ScannerState.FloatLiteral); break;
                default: Machine.SetState (ScannerState.Halt); break;
            }
        }
    }
}

