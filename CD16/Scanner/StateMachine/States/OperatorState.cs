﻿using System;
using CompilerTools.Scanner;
using CompilerTools.StateMachine;

namespace CD16.Scanner.StateMachine.States
{
    public class OperatorState : MachineState<ScannerState>
    {
        // When we are in the initialised state:
        //  - If input is Whitespace, update state to CompleteToken
        //  - If input is Char, update state to Halt
        //  - If input is Numeric, update state to Halt
        //  - If input is Punctuation, update state to CompleteToken or Halt???
        //  - If input is ", update state to Halt

        public OperatorState () { }

        public override void Update (char c)
        {
            Classification _class = InputClassifier.Classify (c);

            switch (_class) {
                case Classification.Whitespace:  Machine.SetState (ScannerState.CompleteToken); break;
                case Classification.Character:   Machine.SetState (ScannerState.Halt); break;
                case Classification.Numeric:     Machine.SetState (ScannerState.Halt); break;
                case Classification.Punctuation: Machine.SetState (ScannerState.Halt); break;
                default:                         Machine.SetState (ScannerState.Halt); break;
            }
        }
    }
}

