﻿using System;
namespace CD16.Scanner
{
    /// <summary>
    /// Enumeration of CD16 Tokens
    /// </summary>
    public enum CD16Token
    {
        // Finish off TokenType enum

        // Keywords
        CD16,
        Constants,
        Types,
        Is,
        Arrays,
        Main,
        Begin,
        End,
        Array,
        Of,
        Func,
        Void,
        Const,
        Integer,
        Real,
        Boolean,
        For,
        Repeat,
        Until,
        If,
        Else,
        In,
        Out,
        Line,
        Return,
        Not,
        And,
        Or,
        Xor,
        True,
        False,

        // Operators
        SemiColon,
        LeftBracket,
        RightBracket,
        Comma,
        LeftParenthesis,
        RightParenthesis,
        Equals,
        Plus,
        Minus,
        Star,
        Slash,
        Percent,
        Carat,
        LessThan,
        GreaterThan,
        Exclamation,
        Quote,
        Colon,
        Dot,

        // Composite Operators
        LessLess,
        GreaterGreater,
        LessThanEqual,
        GreaterThanEqual,
        NotEquals,
        DoubleEquals,

        // Compiler Specific
        Identifier,
        IntegerLiteral,
        FloatLiteral,
        StringLiteral,
        Undefined,
        EndOfFile

    }
}

