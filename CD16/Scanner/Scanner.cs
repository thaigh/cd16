﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using CD16.ErrorHandling;
using CD16.Scanner.StateMachine;
using CD16.Scanner.StateMachine.States;
using CompilerTools.Scanner;
using CompilerTools.StateMachine;
using TokenTuple = CompilerTools.Scanner.TokenTuple<CD16.Scanner.CD16Token>;

namespace CD16.Scanner
{
    public class Scanner : IScanner<CD16Token>
    {
        // Responsibilities:
        //   - Take in a string / stream input
        //   - Cleanse input to simplify reading
        //   - Read character by character
        //   - Update state machine to appropriate state
        //   - Collect all tokens that can be parsed

        private static IDictionary<ScannerState, MachineState<ScannerState>> States = new Dictionary<ScannerState, MachineState<ScannerState>>
        {
            { ScannerState.Initialised, new InitialisedState() },
            { ScannerState.Identifier, new IdentifierState() },
            { ScannerState.CompleteToken, new CompleteTokenState() },
            { ScannerState.Operator, new OperatorState() },
            { ScannerState.PossibleCompositeOperator, new PossibleCompositeOperatorState() },
            { ScannerState.CompositeOperator, new CompositeOperatorState() },
            { ScannerState.Halt, new HaltState() },
            { ScannerState.Invalid, new InvalidState() },
            { ScannerState.PossibleComment, new PossibleCommentState() },
            { ScannerState.Comment, new CommentState() },
            { ScannerState.IntegerLiteral, new IntegerLiteralState() },
            { ScannerState.PossibleFloat, new PossibleFloatState() },
            { ScannerState.FloatLiteral, new FloatLiteralState() },
            { ScannerState.StringLiteral, new StringLiteralState() },
            { ScannerState.TerminatedString, new TerminatedStringState() },
            { ScannerState.EndOfFile, new EndOfFileState() }
        };

        public static ICollection<char> ValidPunctuation { get { return new HashSet<char> (_ValidPunctuation); } }
        private static readonly ICollection<char> _ValidPunctuation = new HashSet<char> {
            ':', ';', '[', ']', '(', ')', '.', ',', '<', '>', '!', '=', '+', '-', '*', '/', '%', '^', '\"'
        };

        private StateMachine<ScannerState> _machine = new StateMachine<ScannerState> (States, ScannerState.Initialised);
        private StringBuilder _lexemeBuffer = new StringBuilder();

        private SourceCodeReader _codeReader;

        public event CompilerErrorEventArgs.CompilerErrorHandler LexicalError;

        public Scanner (FileInfo file) { _codeReader = new SourceCodeReader (file); }
        public Scanner (StreamReader stream) { _codeReader = new SourceCodeReader (stream); }
        public Scanner (string sourceCodeInput) { _codeReader = new SourceCodeReader (sourceCodeInput); }

        public ICollection<TokenTuple> GetAllTokens ()
        {
            ICollection<TokenTuple> tokens = new List<TokenTuple> ();

            while (_codeReader.CanRead) {
                TokenTuple token = GetNextToken ();

                if (token != null) {
                    if (token.TokenType == CD16Token.Undefined)
                        OnLexicalError (token);
                    
                    tokens.Add (token);
                }
            }

            tokens.Add (new TokenTuple (CD16Token.EndOfFile, null, _codeReader.Line, _codeReader.Column));

            return tokens;
        }

        private void OnLexicalError(TokenTuple token)
        {
            if (LexicalError != null)
            {
                CompilerErrorEventArgs args = new CompilerErrorEventArgs {
                    Error = ErrorType.LexicalError,
                    Line = token.LineNumber,
                    Column = token.ColumnNumber,
                    Message = string.Format ("Undefined token '{0}'", token.Lexeme)
                };
                LexicalError (this, args);
            }
        }

        public TokenTuple GetNextValidToken ()
        {
            TokenTuple token = null;

            while (token == null && _codeReader.CanRead) {
                token = GetNextToken ();
            }

            return token ?? new TokenTuple (CD16Token.EndOfFile, null, _codeReader.Line, _codeReader.Column);
        }

        private TokenTuple GetNextToken()
        {
            while (_codeReader.CanRead)
            {
                // Check if we were in halting state and attempt to return Token
                if (_machine.CurrentState == ScannerState.Halt) {
                    return ProcessCompleteToken ((char)0);
                }

                // Read character
                char c = _codeReader.ReadChar ();

                // Update state machine
                _machine.Update (c);

                // Check for return of token
                if (TerminatingState(_machine.CurrentState)) {
                    return ProcessCompleteToken (c);
                }

                // Add to lexeme buffer
                if (ShouldAddToLexemeBuffer(c))
                    _lexemeBuffer.Append (c);

                // Rewind the code reader one character when we are in the halt state,
                // so we can re-read the character again from clean state
                if (_machine.CurrentState == ScannerState.Halt)
                    _codeReader.Rewind ();

                if (_machine.CurrentState == ScannerState.Comment)
                    _lexemeBuffer.Clear ();

            }

            _machine.SetState (ScannerState.EndOfFile);

            return (_lexemeBuffer.ToString ().Length > 0)
                    ? GenerateTokenFromLexemeBuffer ()
                    : null;
                
        }

        private bool ShouldAddToLexemeBuffer(char c)
        {
            // Don't add the character to the buffer if we are halting, as we will rewind and re-read it
            if (_machine.CurrentState == ScannerState.Halt) return false;

            if (  _machine.CurrentState == ScannerState.StringLiteral) return c != '\"';

            if (c == '\n' || c == ' ') return false;

            return true;
        }

        private TokenTuple GenerateTokenFromLexemeBuffer()
        {
            string lexeme = _lexemeBuffer.ToString ();

            TokenTuple token = null;

            if (_machine.PreviousState == ScannerState.FloatLiteral ||
                _machine.CurrentState  == ScannerState.FloatLiteral)
                token = Tokeniser.GenerateToken (float.Parse (lexeme));

            else if (_machine.PreviousState == ScannerState.IntegerLiteral ||
                     _machine.CurrentState  == ScannerState.IntegerLiteral)
                token = Tokeniser.GenerateToken (int.Parse (lexeme));

            else if (_machine.CurrentState == ScannerState.TerminatedString)
                token = Tokeniser.GenerateStringToken (lexeme);

            else if (_machine.PreviousState == ScannerState.StringLiteral)
            {
                token = Tokeniser.GenerateStringToken (lexeme);
                token.TokenType = CD16Token.Undefined;
            }

            else
                // Identifier or Keyword or Operator
                token = Tokeniser.GenerateToken (lexeme);

            // Apply the Column and Line Numbers
            token.LineNumber = _codeReader.Line;
            token.ColumnNumber = _codeReader.Column - lexeme.Length;

            // Handle end of file in the middle of a string
            if (_machine.PreviousState == ScannerState.StringLiteral &&
                _machine.CurrentState == ScannerState.EndOfFile)
                token.ColumnNumber--;

            // Remove lexeme from buffer
            _lexemeBuffer.Remove (0, lexeme.Length);

            return token;
        }

        private bool TerminatingState(ScannerState state)
        {
            return state == ScannerState.CompleteToken ||
                   state == ScannerState.Operator ||
                   state == ScannerState.CompositeOperator ||
                   state == ScannerState.TerminatedString ||
                   state == ScannerState.Invalid;
        }

        private TokenTuple ProcessCompleteToken(char c)
        {
            TokenTuple newToken = null;

            if (_machine.PreviousState == ScannerState.Comment)
            {
                _machine.SetState (ScannerState.Initialised);
                return null;
            }

            if (c == '\n')
                _codeReader.Rewind ();


            // Do any final processing required
            switch (_machine.CurrentState) {
                case ScannerState.Halt:
                {
                    newToken = GenerateTokenFromLexemeBuffer ();

                    if (_machine.PreviousState == ScannerState.PossibleComment ||
                        _machine.PreviousState == ScannerState.PossibleFloat)
                        newToken.TokenType = CD16Token.Undefined;

                    if (_machine.PreviousState == ScannerState.StringLiteral) {
                        newToken.TokenType = CD16Token.Undefined;
                        newToken.ColumnNumber = newToken.ColumnNumber - 1; // To subtract leading " character
                    }

                    break;
                }
                case (ScannerState.Invalid):
                    _lexemeBuffer.Append (c);    
                    newToken = GenerateTokenFromLexemeBuffer ();
                    newToken.TokenType = CD16Token.Undefined;
                    break;
                case ScannerState.Operator:
                case ScannerState.CompositeOperator: {
                    _lexemeBuffer.Append (c);
                    newToken = GenerateTokenFromLexemeBuffer ();
                    break;
                }
                default: {
                    newToken = GenerateTokenFromLexemeBuffer ();
                    if (c != '\n') newToken.ColumnNumber = newToken.ColumnNumber - 1; // To handle the character we just read

                    if (_machine.CurrentState == ScannerState.TerminatedString)
                        newToken.ColumnNumber = newToken.ColumnNumber - 1; // To subtract leading " character

                    break;
                }
            }

            _machine.SetState (ScannerState.Initialised);
            return newToken;
        }


    }
}

