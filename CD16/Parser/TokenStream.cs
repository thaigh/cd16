﻿using System;
using System.Collections.Generic;
using System.Linq;
using CD16.Scanner;
using TokenTuple = CompilerTools.Scanner.TokenTuple<CD16.Scanner.CD16Token>;

namespace CD16.Parser
{
    public class TokenStream
    {

        private IList<TokenTuple> _tokens;
        private int _tokenIndex = 0;

        public TokenStream (IEnumerable<TokenTuple> tokens)
        {
            this._tokens = tokens.ToList();
        }

        public TokenTuple Peek(int amount = 0) {

            int effectiveIndex = _tokenIndex + amount;

            //if ((effectiveIndex >= _tokens.Count) || (effectiveIndex < 0))
            //    throw new ArgumentException ("Attempt to peek outside of token stream bounds");

            return _tokens[effectiveIndex];
        }

        public TokenTuple LastToken()
        {
            return Peek (-1);
        }

        public void Consume(int amount = 1)
        {
            _tokenIndex += amount;
        }

        public bool PeekAndConsume(CD16Token targetType)
        {
            TokenTuple token = Peek ();
            if (token != null && token.TokenType == targetType)
            {
                Consume ();
                return true;
            }

            return false;
        }

    }
}

