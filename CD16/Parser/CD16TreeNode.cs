﻿using System;
using System.Collections.Generic;
using CompilerTools.Parser;
using CompilerTools.Parser.SymbolTable;

namespace CD16.Parser
{
    public class CD16TreeNode : TreeNode
    {
        public TreeNodeType NodeType { get; set; }
        public NodeDataType DataType { get; set; }

        public CD16TreeNode LeftChild { get; set; }
        public CD16TreeNode MiddleChild { get; set; }
        public CD16TreeNode RightChild { get; set; }

        public SymbolTableRecord IdentifierRecord { get; set; }
        //public SymbolTableRecord ConstRecord { get; set; }

        public CD16TreeNode ()
        {
            NodeType = TreeNodeType.Undefined;
            DataType = NodeDataType.Undefined;

            LeftChild = null;
            MiddleChild = null;
            RightChild = null;

            IdentifierRecord = null;
            //ConstRecord = null;
        }

        public CD16TreeNode (TreeNodeType nodeType) : this()
        {
            this.NodeType = nodeType;
        }

        public CD16TreeNode (TreeNodeType nodeType, SymbolTableRecord identifierRecord)
            : this(nodeType)
        {
            this.IdentifierRecord = identifierRecord;
        }

        public CD16TreeNode (TreeNodeType nodeType, CD16TreeNode left, CD16TreeNode right)
            : this(nodeType)
        {
            this.LeftChild = left;
            this.RightChild = right;
        }

        public CD16TreeNode (TreeNodeType nodeType, CD16TreeNode left, CD16TreeNode middle, CD16TreeNode right)
            : this(nodeType, left, right)
        {
            this.MiddleChild = middle;
        }

        public override string ToString ()
        {
            return NodeType
                    + " [" + DataType + "]"
                    + ((IdentifierRecord != null) ? " " + IdentifierRecord.Lexeme : "")
                    //+ ((ConstRecord != null) ? " " + ConstRecord.Lexeme : "")
                ;
        }

        public string PrintTree ()
        {
            return PrintTree ("", true);
        }

        private string PrintTree (string prefix, bool isTail)
        {

            string result = prefix + (isTail ? "\\--- " : "|--- ") + ToString () + "\n";

            CD16TreeNode l = LeftChild;
            CD16TreeNode m = MiddleChild;
            CD16TreeNode r = RightChild;

            IList<CD16TreeNode> children = new List<CD16TreeNode> ();

            if (l != null) children.Add (l);
            if (m != null) children.Add (m);
            if (r != null) children.Add (r);

            for (int i = 0; i < children.Count - 1; i++) {
                result += children[i].PrintTree (prefix + (isTail ? "     " : "|    "), false);
            }

            if (children.Count > 0) {
                result += children[children.Count - 1].PrintTree (prefix + (isTail ? "     " : "|    "), true);
            }

            return result;
        }

        public override bool Equals (object obj)
        {
            if (this == obj) return true;
            if (this.GetType() != obj.GetType()) return false;

            CD16TreeNode other = (CD16TreeNode)obj;

            if (this.NodeType != other.NodeType) return false;
            if (this.DataType != other.DataType) return false;

            if (this.LeftChild   != null ? !LeftChild.Equals(other.LeftChild)     : other.LeftChild   != null) return false;
            if (this.MiddleChild != null ? !MiddleChild.Equals(other.MiddleChild) : other.MiddleChild != null) return false;
            if (this.RightChild  != null ? !RightChild.Equals(other.RightChild)   : other.RightChild  != null) return false;

            if (this.IdentifierRecord != null ? !IdentifierRecord.Equals(other.IdentifierRecord) : other.IdentifierRecord != null) return false;

            return true;
        }

        public override int GetHashCode ()
        {
            int result = NodeType.GetHashCode ();
            result = 31 * result + DataType.GetHashCode ();

            result = 31 * result + (LeftChild   != null ? LeftChild.GetHashCode ()   : 0);
            result = 31 * result + (MiddleChild != null ? MiddleChild.GetHashCode () : 0);
            result = 31 * result + (RightChild  != null ? RightChild.GetHashCode ()  : 0);

            result = 31 * result + (IdentifierRecord != null ? IdentifierRecord.GetHashCode () : 0);
            //result = 31 * result + (constRecord != null ? constRecord.hashCode () : 0);

            return result;
        }

    }
}

