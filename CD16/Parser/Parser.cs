﻿using System;
using System.Collections.Generic;
using CD16.ErrorHandling;
using CD16.Parser.Nodes;
using CD16.Scanner;
using CompilerTools.Parser;
using CompilerTools.Parser.SymbolTable;
using CompilerTools.Scanner;
using TokenTuple = CompilerTools.Scanner.TokenTuple<CD16.Scanner.CD16Token>;

namespace CD16.Parser
{
    public class Parser : IParser<CD16Token>
    {
        private SymbolTable _constants = new SymbolTable ();
        private SymbolTable _identifiers = new SymbolTable();
        private TokenStream _tokenStream;

        private ScopeStack _programScope = new ScopeStack (0);

        public event CompilerErrorEventArgs.CompilerErrorHandler SyntacticError;
        public event CompilerErrorEventArgs.CompilerErrorHandler SemanticError;

        public Parser ()
        {
        }

        public TreeNode Parse (ICollection<TokenTuple> tokens)
        {
            _tokenStream = new TokenStream (tokens);

            return new ProgramNode ().Make (this);
            //throw new NotImplementedException ();
        }

        public void ConsumeToken (int amount = 1)
        {
            _tokenStream.Consume (amount);
        }

        public TokenTuple Peek (int amount = 0)
        {
            return _tokenStream.Peek (amount);
        }

        public bool PeekAndConsume (CD16Token targetTokenType)
        {
            return _tokenStream.PeekAndConsume (targetTokenType);
        }

        public TokenTuple LastToken()
        {
            return _tokenStream.Peek (-1);
        }

        public void InsertConstant(TokenTuple constantToken)
        {
            InsertToken (constantToken, _constants);
        }

        public void InsertIdentifier(TokenTuple identifierToken)
        {
            InsertToken (identifierToken, _identifiers);
        }

        private void InsertToken(TokenTuple token, SymbolTable destinationTable)
        {
            CD16SymbolMeta meta = new CD16SymbolMeta ();
            SymbolTableRecord rec = new SymbolTableRecord (token.Lexeme, _programScope.ActiveScope ()) { Properties = meta };

            if (_constants.Contains (rec)) rec = _constants.Lookup (rec);
            else {
                CD16SymbolMeta metadata = (CD16SymbolMeta)rec.Properties;
                metadata.TokenType = token.TokenType;
                _constants.Insert (rec);
            }

            // Set constant token's Symbol Table Record
            token.SymbolTableRecord = rec;
        }

        public SymbolTableRecord LookupConstant(string lexeme)
        {
            return LookupSymbolTable (lexeme, _constants);
        }

        public SymbolTableRecord LookupIdentifier(string lexeme)
        {
            return LookupSymbolTable (lexeme, _identifiers);
        }

        private SymbolTableRecord LookupSymbolTable(string lexeme, SymbolTable table)
        {
            return table.Lookup (_programScope.ActiveScope (), lexeme);
        }

        public void GenerateSyntacticError(string message, int line, int column)
        {
            if (SyntacticError != null)
            {
                CompilerErrorEventArgs args = new CompilerErrorEventArgs {
                    Message = message, Line = line, Column = column,
                    Error = ErrorType.SyntacticError
                };

                SyntacticError (this, args);
            }
        }

        public void GenerateSemanticError (string message, int line, int column)
        {
            if (SemanticError != null) {
                CompilerErrorEventArgs args = new CompilerErrorEventArgs {
                    Message = message, Line = line, Column = column,
                    Error = ErrorType.SemanticError
                };

                SemanticError (this, args);
            }
        }

        public void PushProgramScope(string scope) {
            _programScope.PushScope (scope);
        }

        public string PopProgramScope () {
            return _programScope.PopScope ();
        }

        public bool PeekAndConsumeKeyword (CD16Token token)
        {
            if (!PeekAndConsume (token)) {
                GenerateSyntacticError (
                    string.Format ("Expected keywork '{0}'. Found {1}.", token, Peek().TokenType),
                    Peek ().LineNumber,
                    Peek ().ColumnNumber);
                return false;
            }

            return true;
        }

        public bool PeekAndConsume (CD16Token token, string message)
        {
            if (!PeekAndConsume (token)) {
                GenerateSyntacticError (
                    message,
                    Peek ().LineNumber,
                    Peek ().ColumnNumber);
                return false;
            }

            return true;
        }

    }
}

