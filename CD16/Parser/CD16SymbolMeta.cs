﻿using System;
using CD16.Scanner;
using CompilerTools.Parser.SymbolTable;

namespace CD16.Parser
{
    public class CD16SymbolMeta : SymbolTableMetadata
    {
        public CD16Token TokenType { get; set; }
        public CD16TreeNode TreeNode { get; set; }

        public CD16SymbolMeta ()
        {
            TokenType = CD16Token.Undefined;
            TreeNode = null;
        }

        public override bool Equals (object obj)
        {
            if (this == obj) return true;
            if (this.GetType () != obj.GetType ()) return false;

            if (!base.Equals (obj)) return false;

            CD16SymbolMeta sym = (CD16SymbolMeta)obj;

            if (this.TokenType != sym.TokenType) return false;
            if (this.TreeNode != null
                ? !TreeNode.Equals (sym.TreeNode)
                : sym.TreeNode != null) return false;

            return true;
        }

        public override int GetHashCode ()
        {
            int result = base.GetHashCode();

            result = 31 * result + (TokenType.GetHashCode ());
            result = 31 * result + (TreeNode != null ? TreeNode.GetHashCode () : 0);
            return result;
        }
    }
}

