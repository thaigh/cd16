﻿using System;
using System.Collections.Generic;

namespace CD16.Parser
{
    public class ScopeStack
    {

        private Stack<string> _stack = new Stack<string> ();
        private int _minStackSize;

        public ScopeStack (int minStackSize) { _minStackSize = minStackSize; }

        public void PushScope (string scope) { _stack.Push (scope); }
        public string PopScope () { return _stack.Pop (); }
        public bool EmptyScope () { return _stack.Count <= _minStackSize; }
        public string ActiveScope () { return _stack.Peek (); }
    }
}

