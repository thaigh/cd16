﻿using System;
namespace CD16.Parser.Nodes.Globals
{
    public class GlobalsNode : CD16TreeNode, INodeGenerator
    {
        public GlobalsNode ()
        {
        }

        public CD16TreeNode Make (Parser p)
        {
            // <globals> := <consts> <types> <arrays>

            CD16TreeNode consts = null;
            CD16TreeNode types = null;
            CD16TreeNode arrays = null;

            if (p.Peek().TokenType == Scanner.CD16Token.Constants)
            {
                consts = new ConstantsNode ().Make (p);
                if (InvalidNode (consts) && !ErrorRecovery (p)) { return FailedNode (p); }
            }

            if (p.Peek().TokenType == Scanner.CD16Token.Types)
            {
                types = new TypesNode ().Make (p);
                if (InvalidNode (types) && !ErrorRecovery (p)) { return FailedNode (p); }
            }

            if (p.Peek().TokenType == Scanner.CD16Token.Arrays)
            {
                arrays = new ArraysNode ().Make (p);
                if (InvalidNode (arrays) && !ErrorRecovery (p)) { return FailedNode (p); }
            }

            GlobalsNode globals = new GlobalsNode {
                NodeType = TreeNodeType.Globals,
                LeftChild = consts,
                MiddleChild = types,
                RightChild = arrays
            };

            // TODO: Perform semantic checks?

            return globals;
        }

        private bool InvalidNode (CD16TreeNode node)
        {
            return node != null && node.NodeType == TreeNodeType.Undefined;
        }

        private bool ErrorRecovery (Parser p)
        {
            return true;
        }

        private CD16TreeNode FailedNode (Parser p)
        {
            //p.PopProgramScope ();
            return new GlobalsNode ();
        }
    }
}

