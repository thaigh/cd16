﻿using System;
namespace CD16.Parser.Nodes
{
    public class MainBodyNode : CD16TreeNode, INodeGenerator
    {
        public MainBodyNode ()
        {
        }

        public CD16TreeNode Make (Parser p)
        {
            return new MainBodyNode  { NodeType = TreeNodeType.MainBody };
        }
    }
}

