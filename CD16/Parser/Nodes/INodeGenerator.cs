﻿using System;
namespace CD16.Parser.Nodes
{
    public interface INodeGenerator
    {
        CD16TreeNode Make (Parser p);
    }
}

