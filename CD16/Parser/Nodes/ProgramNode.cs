﻿using System;
using CD16.Parser.Nodes.Globals;
using CD16.Scanner;
using TokenTuple = CompilerTools.Scanner.TokenTuple<CD16.Scanner.CD16Token>;

namespace CD16.Parser.Nodes
{
    public class ProgramNode : CD16TreeNode, INodeGenerator
    {

        // Additional properties
        public ProgramNode () { }

        // INodeGenerator  Implementation

        public CD16TreeNode Make (Parser p)
        {
            // <program> := CD16 <id> <globals> <funcs> <mainBody> CD16 <id>

            p.PushProgramScope ("cd16");

            if (!p.PeekAndConsumeKeyword (CD16Token.CD16)) { return FailedNode(p); }
            if (!p.PeekAndConsume (CD16Token.Identifier, "Expected program name identifier")) { return FailedNode (p); }

            TokenTuple firstIdentifier = p.LastToken ();
            p.InsertIdentifier (firstIdentifier);

            CD16TreeNode globals = new GlobalsNode ().Make (p);
            if (InvalidNode(globals) && !ErrorRecovery(p)) { return FailedNode (p); }

            CD16TreeNode funcs = new FunctionsNode ().Make (p);
            if (InvalidNode (funcs) && !ErrorRecovery (p)) { return FailedNode (p); }

            CD16TreeNode mainBody = new MainBodyNode ().Make (p);
            if (InvalidNode (mainBody) && !ErrorRecovery (p)) { return FailedNode (p); }

            if (!p.PeekAndConsumeKeyword (CD16Token.CD16)) { return FailedNode (p); }
            if (!p.PeekAndConsume (CD16Token.Identifier, "Expected program name identifier")) { return FailedNode (p); }

            TokenTuple secondIdentifier = p.LastToken ();
            p.InsertIdentifier (secondIdentifier);

            ProgramNode programNode = new ProgramNode {
                NodeType = TreeNodeType.Program,
                LeftChild = globals, MiddleChild = funcs, RightChild = mainBody,
                IdentifierRecord = firstIdentifier.SymbolTableRecord

            };

            if (!SemanticCheckProgramIds(firstIdentifier, secondIdentifier))
            {
                p.GenerateSemanticError (
                    string.Format (
                        "The program identifiers do not match. [{0} - {1}]",
                        firstIdentifier.Lexeme,
                        secondIdentifier.Lexeme),
                    secondIdentifier.LineNumber, secondIdentifier.ColumnNumber);
            } else {
                programNode.DataType = NodeDataType.Program;
            }

            return programNode;
        }

        private CD16TreeNode FailedNode(Parser p)
        {
            p.PopProgramScope ();
            return new ProgramNode ();
        }

        private bool SemanticCheckProgramIds(TokenTuple firstIdentifier, TokenTuple secondIdentifier)
        {
            return (firstIdentifier.SymbolTableRecord == secondIdentifier.SymbolTableRecord);
        }

        private bool InvalidNode(CD16TreeNode node)
        {
            return node != null && node.NodeType == TreeNodeType.Undefined;
        }

        private bool ErrorRecovery(Parser p)
        {
            return true;
        }


    }
}

