﻿using System;
namespace CD16.Parser.Nodes
{
    public class FunctionsNode : CD16TreeNode, INodeGenerator
    {
        public FunctionsNode ()
        {
        }

        public CD16TreeNode Make (Parser p)
        {
            return new FunctionsNode { NodeType = TreeNodeType.FuncsList };
        }
    }
}

