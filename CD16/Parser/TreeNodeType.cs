﻿using System;
namespace CD16.Parser
{
    public enum TreeNodeType
    {
        // Finish off TreeNodeType Type enumeration

        Undefined,
        Program,

        Globals,
        GlobalConsts,
        GlobalTypes,
        GlobalArrays,

        // Global Consts
        ConstInitList,
        Initialisation,

        // Global Types
        TypesList,
        StructTypeDeclaration,
        ArrayTypeDeclaration,
        Fields,

        // Global Arrays
        ArrayDeclarationList,
        ArrayDeclaration,

        // Funcs
        FuncsList,
        Func,
        // ReturnType, // Use S_Type or Null (meaning Void)
        ParameterList,
        //Params,
        Parameter,
        FunctionBody,
        //Locals,
        LocalDeclarationList,
        LocalDeclaration,
        ConstArray,

        MainBody,
        S_DeclarationList,
        S_Declaration,
        S_Type,

        StatementList,

        // Statements
        ForStatement,
        IfStatement,
        IfElseStatement,
        RepeatStatement,
        AssignStatement,
        IoInStatement,
        IoOutStatement,
        CallStatement,
        ReturnStatement,

        AssignmentList,

        VariableList,
        Variable,

        ExpressionList,

        // Boolean Expressions
        AndExpression,
        OrExpression,
        XorExpression,
        NotExpression,

        // Relational Expressions
        EqualityExpression,
        NotEqualsExpression,
        GreaterExpression,
        LessExpression,
        GreaterEqualsExpression,
        LessEqualsExpression,

        // Terms / Facts / Exponents
        AddExpression,
        SubtractExpression,
        MultiplyExpression,
        DivideExpression,
        ModuloExpression,
        ExponentExpression,

        IntegerLiteralId,
        RealLiteralId,
        FunctionCall,
        TrueLiteral,
        FalseLiteral,

        PrintList,
        StringLiteralId

    }
}

