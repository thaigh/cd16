﻿using System;
namespace CD16.Parser
{
    public enum NodeDataType
    {
        Undefined,
        Float,
        Int,
        String,
        Boolean,
        List,
        Node,
        Program,
        Function
    }
}

