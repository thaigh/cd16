﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CD16.ErrorHandling;
using CD16.Scanner;
using CompilerTools;
using CompilerTools.Parser;
using CompilerTools.Scanner;
using TokenTuple = CompilerTools.Scanner.TokenTuple<CD16.Scanner.CD16Token>;

namespace CD16
{
    public class Compiler : ICompiler<CD16Token>
    {

        private CompilerOptions _options;

        private Scanner.Scanner _scanner;
        private Parser.Parser _parser;

        private bool _lexicallyValid     = true;
        private bool _syntacticallyValid = true;
        private bool _semanticallyValid  = true;

        private ErrorReporter _errorReporter;

        public Compiler (CompilerOptions options)
        {
            _options = options;

            FileInfo file = new FileInfo (options.InputFile);
            _scanner = new Scanner.Scanner (file);

            _parser = new Parser.Parser ();

            _errorReporter = new ErrorReporter (file);

            _scanner.LexicalError  += CompilerError;
            _parser.SyntacticError += CompilerError;
            _parser.SemanticError  += CompilerError;
        }

        public void Compile ()
        {
            var tokens = LexicalAnalysis ();
            var syntaxTree = SyntacticAnalysis (tokens);

            if (_lexicallyValid && _syntacticallyValid && _semanticallyValid)
                CodeGeneration (syntaxTree);
        }

        public ICollection<TokenTuple> LexicalAnalysis ()
        {
            var tokens = _scanner.GetAllTokens ();

            if (_options.Verbose)
                foreach (var t in tokens)
                    Console.WriteLine (t);

            // Remove all Undefined tokens. Requires extension method from Stack Overflow
            bool removed = tokens.RemoveAll (t => t.TokenType == CD16Token.Undefined);
            _lexicallyValid = !removed;

            return tokens;
        }

        public TreeNode SyntacticAnalysis (ICollection<TokenTuple> tokens)
        {
            return _parser.Parse (tokens);
        }

        public string CodeGeneration (TreeNode syntaxTree)
        {
            return new string (new char [] { (char)0 });
        }

        private void CompilerError (object sender, CompilerErrorEventArgs args)
        {
            _errorReporter.AddError (args);
        }
    }
}

