﻿using System;
namespace CD16.ErrorHandling
{
    public enum ErrorType
    {
        LexicalError,
        SyntacticError,
        SemanticError
    }
}

