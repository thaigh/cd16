﻿using System;
namespace CD16.ErrorHandling
{
    public class CompilerErrorEventArgs : EventArgs
    {
        public delegate void CompilerErrorHandler (object sender, CompilerErrorEventArgs args);

        public ErrorType Error { get; set; }
        public string Message { get; set; }
        public int Line { get; set; }
        public int Column { get; set; }
    }
}

