﻿using System;
using System.Collections.Generic;
using System.IO;
using CD16.ErrorHandling;
using CompilerTools.Scanner;

namespace CD16.ErrorHandling
{
    public class ErrorReporter
    {

        private ICollection<CompilerErrorEventArgs> _errors = new List<CompilerErrorEventArgs>();
        private SourceCodeReader _codeReader;

        public ErrorReporter (FileInfo file) { _codeReader = new SourceCodeReader (file); }
        public ErrorReporter (StreamReader stream) { _codeReader = new SourceCodeReader (stream); }
        public ErrorReporter (string sourceCodeInput) { _codeReader = new SourceCodeReader (sourceCodeInput); }

        public void AddError (CompilerErrorEventArgs args)
        {
            _errors.Add (args);
        }

    }
}

