﻿using System;
using System.Collections.Generic;
using CommandLine;

namespace CD16
{
    class MainClass
    {
        public static void Main (string [] args)
        {
            CommandLine.Parser.Default.ParseArguments<CompilerOptions> (args)
                .WithParsed (RunCompiler)
                //.WithNotParsed (DisplayErrors)
                ;
        }

        private static void RunCompiler(CompilerOptions options)
        {
            Compiler c = new Compiler (options);
            c.Compile ();
        }

        private static void DisplayErrors(IEnumerable<Error> errors)
        {
            foreach (var e in errors)
                Console.WriteLine (e);
        }
    }
}
