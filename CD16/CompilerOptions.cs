﻿using System;
using CommandLine;

namespace CD16
{
    public class CompilerOptions
    {
        [Option ('i', "input", HelpText="The input source code file to compile", Required=true)]
        public string InputFile { get; set; }

        [Option ('o', "output", HelpText="The output file to write the code module to", Required=true)]
        public string OutputFile { get; set; }

        [Option ('v', "verbose", HelpText="Write intermediate outputs to the console", Required=false, Default=false)]
        public bool Verbose { get; set; }
    }
}

